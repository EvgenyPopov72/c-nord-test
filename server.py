#!/usr/bin/env python

import logging
import struct

import time
from collections import namedtuple

from tornado.ioloop import IOLoop
from tornado import gen
from tornado.iostream import StreamClosedError
from tornado.tcpserver import TCPServer
from tornado.options import options, define

define("port1", default=8888, help="TCP port to listen server1 on")
define("port2", default=8889, help="TCP port to listen server2 on")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

SOURCE_STATUS = {0x01: 'IDLE', 0x02: 'ACTIVE', 0x03: 'RECHARGE'}

HEADER_STRUCT = ">BH8sBB"
HEADER_FIELDS = ['header', 'message_id', 'source_id', 'source_status', 'num_fields']

RESPONSE_STRUCT = ">BH"
RESPONSE_HEADER = (0x11, 0x12)


def b_xor(a):
    result = 0
    for ch in a:
        result ^= ch
    return result


class Message:
    def __init__(self, message_id, source_id, source_status, fields, timestamp=None):
        self.message_id = message_id
        self.source_id = source_id.decode("utf8")
        self.source_status = SOURCE_STATUS.get(source_status)
        self.fields = fields
        self.timestamp = timestamp or int(time.time() * 1000)

    def __str__(self):
        return "[%s   %s, %s, %s, %s]" % (
                self.timestamp, self.message_id, self.source_id, self.source_status, self.fields)


class Storage:
    def __init__(self):
        self.messages = dict()
        self.subscribers = set()

    def add_msg(self, source_id, msg):
        if msg is None or source_id is None:
            return
        msg_list = self.messages.get(source_id, [])
        msg_list.append(msg)
        self.messages[source_id] = msg_list

        for callback in self.subscribers:
            IOLoop.current().spawn_callback(callback, msg)

    def get_last_messages(self):
        return [(msgs[-1] if msgs else None) for _, msgs in self.messages.items()]

    def add_source(self, src):
        if src not in self.messages:
            self.messages[src] = []

    def del_source(self, src):
        self.messages.pop(src, None)

    def subscribe(self, callback):
        if callable(callback):
            self.subscribers.add(callback)

    def unscribe(self, callback):
        if callback in self.subscribers:
            self.subscribers.remove(callback)

    def __str__(self):
        return ", ".join(self.messages.keys())


class SourcesServerHandler(object):
    def __init__(self, stream=None, storage=None):
        self.stream = stream
        self.stream.set_close_callback(self.on_disconnect)

        self.storage = storage

        self.client_id = None
        self.message_id = None
        self.message_error = False

    @gen.coroutine
    def on_disconnect(self):
        self.storage.del_source(self.client_id)
        logger.debug("Disconnected: %s" % self.client_id)
        logger.debug("Connected clients: %s", self.storage)
        yield []

    @gen.coroutine
    def send_response(self):
        header = RESPONSE_HEADER[self.message_error]
        data = struct.pack(RESPONSE_STRUCT, header, self.message_id)
        data += struct.pack(">B", b_xor(data))
        if not self.stream.closed():
            yield self.stream.write(data)
        logger.debug("Send response: %s", data)

    @gen.coroutine
    def dispatch_client(self):
        while True:
            try:
                self.message_error = False
                data = yield self.stream.read_bytes(struct.calcsize(HEADER_STRUCT))

                Header = namedtuple("Header", HEADER_FIELDS)
                header = Header._make(struct.unpack(HEADER_STRUCT, data))
                if not header.header == 0x01 or header.source_status not in SOURCE_STATUS:
                    logger.error("Header error")
                    self.message_error = True

                self.message_id = header.message_id

                FIELD_STRUCT = ">%sB" % ("8sI" * header.num_fields)
                data1 = yield self.stream.read_bytes(struct.calcsize(FIELD_STRUCT))
                unpacked = struct.unpack(FIELD_STRUCT, data1)
                if not b_xor(data + data1[:-1]) == unpacked[-1]:
                    logger.error("XOR error")
                    self.message_error = True

                fields = dict((unpacked[i].decode("utf8"), unpacked[i + 1]) for i in range(0, len(unpacked) - 1, 2))
                msg = Message(header.message_id, header.source_id, header.source_status, fields)
                self.storage.add_msg(self.client_id, msg)

                yield self.send_response()
            except StreamClosedError:
                break
            except Exception as e:
                logger.error("Error: %s", e)

    @gen.coroutine
    def on_connect(self):
        self.client_id = "%s:%d" % self.stream.socket.getpeername()[:2]
        self.storage.add_source(self.client_id)
        logger.debug("Connected: %s", self.client_id)
        logger.debug("Connected clients: %s", self.storage)
        yield self.dispatch_client()


class SourcesServer(TCPServer):
    def __init__(self, storage=None):
        super().__init__()
        self.storage = storage

    @gen.coroutine
    def handle_stream(self, stream, address):
        connection = SourcesServerHandler(stream=stream, storage=storage)
        yield connection.on_connect()


class ApplicationServerHandler(object):
    def __init__(self, stream=None, storage=None):
        self.stream = stream
        self.stream.set_close_callback(self.on_disconnect)

        self.storage = storage
        self.client_id = None

    @gen.coroutine
    def on_disconnect(self):
        logger.debug("Disconnected: %s" % self.client_id)
        yield []

    @gen.coroutine
    def on_connect(self):
        self.client_id = "%s:%d" % self.stream.socket.getpeername()
        logger.debug("Connected: %s", self.client_id)
        self.storage.subscribe(self.send_notify)
        yield self.send_status()

    @gen.coroutine
    def send_notify(self, msg):
        logger.debug("Got msg: %s", msg)

        msgs = ["[%s] %s | %s \r\n" % (msg.source_id, k, v) for k, v in msg.fields.items()]
        data = "".join(msgs).encode("utf8")
        if not self.stream.closed():
            yield self.stream.write(data)

    @gen.coroutine
    def send_status(self):
        last_msgs = self.storage.get_last_messages()
        if not last_msgs:
            return
        cur_time = int(time.time() * 1000)
        msgs = ["[%s] %s | %s | %s\r\n" %
                (m.source_id, m.message_id, m.source_status, cur_time - m.timestamp) for m in last_msgs]
        data = "".join(msgs).encode("utf8")
        if not self.stream.closed():
            yield self.stream.write(data)


class ApplicationServer(TCPServer):
    def __init__(self, storage=None):
        super().__init__()
        self.storage = storage

    @gen.coroutine
    def handle_stream(self, stream, address):
        connection = ApplicationServerHandler(stream=stream, storage=storage)
        yield connection.on_connect()


if __name__ == "__main__":
    options.parse_command_line()

    storage = Storage()

    server1 = SourcesServer(storage)
    server1.listen(options.port1)
    logger.info("Listening on TCP port %d", options.port1)

    server2 = ApplicationServer(storage)
    server2.listen(options.port2)
    logger.info("Listening on TCP port %d", options.port2)

    try:
        IOLoop.current().start()
    except KeyboardInterrupt:
        logger.info("Stopping...")
        IOLoop.current().stop()
